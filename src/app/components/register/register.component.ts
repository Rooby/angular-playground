import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, ValidatorFn, AbstractControl } from "@angular/forms";

import { RegisterService } from '../../webservices/register/register.service';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/webservices/login/login.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  registerForm: FormGroup;

  constructor(private registerService: RegisterService, private loginService: LoginService, private router: Router) { }

  ngOnInit(): void {
    this.registerForm = new FormGroup({
      name: new FormControl('', [ Validators.required]),
      email: new FormControl('',
        { 
          updateOn: "change", 
          validators: [ Validators.required, Validators.email],
          asyncValidators: this.checkUniqueEmail.bind(this)
        },
      ),
      password: new FormControl('', {
        validators: [Validators.required, Validators.minLength(6)]
      }),
      confirmPassword: new FormControl('', {
        validators: [Validators.required, Validators.minLength(6)]
      }),
    }, 
      this.validateConfirmPassword
    );
  }

  get name() { return this.registerForm.get('name'); }

  get email() { return this.registerForm.get('email'); }

  get password() { return this.registerForm.get('password'); }

  get confirmPassword() { return this.registerForm.get('confirmPassword'); }

  validateConfirmPassword(formGroup: FormGroup): ValidatorFn {
    if (formGroup.get("confirmPassword").errors && !formGroup.get("confirmPassword").errors.confirmPassword) {
      return;
    }

    if (formGroup.get("password").value !== formGroup.get("confirmPassword").value) {
      formGroup.get("confirmPassword").setErrors({ "confirmPassword": true});
    } else {
      formGroup.get("confirmPassword").setErrors(null);
    }
  }

  checkUniqueEmail(control: AbstractControl) {
    return this.registerService
      .checkUniqueEmail(control.value)
      .pipe(
        map((response: any) => {
          if (response.exists) {
            return { "uniqueEmail": true};
          } else {
            return null;
          }
        })
      );
  }

  onSubmit() {
    this.registerService
      .register(this.registerForm.value)
      .subscribe( user => {
        this.loginService
          .login(this.registerForm.value)
          .subscribe(
            token => {
              this.router.navigate(["dashboard"]);
            }
          )
      });
  }
}
