import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';

import { UserService } from 'src/app/webservices/user/user.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  editUserForm: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private userService: UserService) { }

  ngOnInit(): void {
    this.editUserForm = new FormGroup({
      name: new FormControl(this.data.user.name, [ Validators.required]),
      email: new FormControl(this.data.user.email,
        { 
          updateOn: "change", 
          validators: [ Validators.required, Validators.email],
          asyncValidators: this.checkUniqueEmail.bind(this)
        },
      ),

    });
  }

  get name() { return this.editUserForm.get('name'); }

  get email() { return this.editUserForm.get('email'); }

  onSubmit() {
    this.userService
      .update(this.editUserForm.value, this.data.user.id)
      .subscribe()
  }

  checkUniqueEmail(control: AbstractControl) {
    return this.userService
      .checkUniqueEmail(control.value, this.data.user.id)
      .pipe(
        map((response: any) => {
          if (response.error) {
            return { "uniqueEmail": true};
          } else {
            return null;
          }
        })
      );
  }
}
