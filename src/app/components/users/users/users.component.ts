import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';

import { UserService } from '../../../webservices/user/user.service';

import { User } from 'src/app/models/user.model';

import { DeleteUserComponent } from '../../dialogs/delete-user/delete-user.component';
import { EditUserComponent } from '../../dialogs/edit-user/edit-user.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'email', 'actions'];
  searchAbleColumns: string[] = ['id', 'name', 'email'];
  sortColumn: string = "id";
  sortDirection: string = "asc";
  dataSource: MatTableDataSource<User>;

  filter: string;
  length: number;
  pageSize: number = 100;
  pageIndex: number = 0;

  isLoading: boolean = true;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private userService: UserService, public dialog: MatDialog) {
    
  }

  ngOnInit(): void {
    this.loadData(this.pageSize, this.pageIndex, this.filter, this.searchAbleColumns, this.sortColumn, this.sortDirection);
  }

  applyFilter(event: Event) {
    this.filter = (event.target as HTMLInputElement).value;
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    this.loadData(this.pageSize, this.pageIndex, this.filter, this.searchAbleColumns, this.sortColumn, this.sortDirection);
  }

  changePage(event){
    this.pageSize = event.pageSize;
    this.loadData(this.pageSize, event.pageIndex, this.filter, this.searchAbleColumns, this.sortColumn, this.sortDirection);
  }

  loadData(pageSize: number, pageIndex: number, filter: string, searchAbleColumns: string[], sortColumn: string, sortDirection: string) {
    this.isLoading = true;
    this.userService
      .list(pageSize, pageIndex, filter, searchAbleColumns, sortColumn, sortDirection)
      .subscribe(
        response => {
          this.dataSource = new MatTableDataSource(response.data);
          this.length = response.filtered;
          this.isLoading = false;
        }
      )
  }

  sortData(sort: Sort) {
    this.sortColumn = sort.active;
    this.sortDirection = sort.direction;
    this.loadData(this.pageSize, this.pageIndex, this.filter, this.searchAbleColumns, this.sortColumn, this.sortDirection);
  }

  openEditUserDialog(user) {
    const dialogRef = this.dialog.open(EditUserComponent, {
      data: { user: user}
    });
    dialogRef.afterClosed().subscribe(
      confirm => {
        if (confirm) {
          this.loadData(this.pageSize, this.pageIndex, this.filter, this.searchAbleColumns, this.sortColumn, this.sortDirection);
        }
      }
    )
  }

  confirmDeleteDialog(user) {
    const dialogRef = this.dialog.open(DeleteUserComponent, {
      data: { user: user}
    });
    dialogRef.afterClosed().subscribe(
      confirm => {
        if (confirm) {
          this.delete(user);
        }
      }
    );
  }

  delete(data) {
    this.userService
      .delete(data.id)
      .subscribe(response => {
        this.loadData(this.pageSize, this.pageIndex, this.filter, this.searchAbleColumns, this.sortColumn, this.sortDirection);
      });
  }
}
