import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, BehaviorSubject, EMPTY } from 'rxjs';
import * as moment from "moment";

import { LoginService } from '../../webservices/login/login.service';
import { switchMap, catchError } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  private isRefreshingSubject = new BehaviorSubject<boolean>(false);

  constructor(private loginService: LoginService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const token = this.loginService.tokenSubject.getValue();
    const now = moment();
    const expiresAt = moment(parseInt(localStorage.getItem("expiresAt")));

    if (token && !this.isRefreshingSubject.getValue()) {
      if (now.unix() < expiresAt.unix()) {
        return next.handle(this.addAuthorizationHeader(request, token));
      } else {
          if (!this.isRefreshingSubject.getValue()) {
            this.isRefreshingSubject.next(true);
            return this.loginService
              .refreshToken(token.refresh_token)
              .pipe(
                switchMap(
                  token => {
                    this.isRefreshingSubject.next(false);
                    return next.handle(this.addAuthorizationHeader(request, token));
                  }
                ),
                catchError(error => {
                  this.loginService.logout();
                  return EMPTY;
                })
              );
          }
      }
    } else if (request.url.endsWith("token") || request.headers.has("whitelist")) {
      return next.handle(request);
    }
    return EMPTY;
  }

  addAuthorizationHeader(request, token) {
    return request.clone({
      headers: request.headers.set("Authorization", "Bearer " + token.access_token)
    });
  }
}
