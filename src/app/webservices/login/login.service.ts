import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable, BehaviorSubject } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import * as moment from 'moment';

import { Token } from '../../models/token.model';
import { Env } from '../../config/env';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  tokenSubject = new BehaviorSubject<Token>(JSON.parse(localStorage.getItem("token")));
  isLoggedSubject = new BehaviorSubject<boolean>(JSON.parse(localStorage.getItem("token")) ? true : false);

  constructor(private http: HttpClient, private router: Router) { }

  login(data: any): Observable<Token> {
    const url = Env.APIurl + "/oauth/token";
  
    const body = {
      grant_type: "password",
      client_id: Env.client_id,
      client_secret: Env.client_secret,
      email: data.email,
      password: data.password,
      scope: ""
    }

    return this.http.post<Token>(url, body).pipe(
      tap(token => {
        const expiresAt = moment().add(token.expires_in, "seconds");
        localStorage.setItem("token", JSON.stringify(token));
        localStorage.setItem("expiresAt", expiresAt.valueOf().toString());

        this.tokenSubject.next(token);
        this.isLoggedSubject.next(true);
      }),
      catchError(this.handleError)
    );
  }

  refreshToken(refreshToken: string): Observable<Token> {
    localStorage.removeItem("token");
    localStorage.removeItem("expiresAt");
    this.tokenSubject.next(null);

    const url = Env.APIurl + "/oauth/token";

    const body = {
      grant_type: "refresh_token",
      refresh_token: refreshToken,
      client_id: Env.client_id,
      client_secret: Env.client_secret,
      scope: ""
    }

    return this.http.post<Token>(url, body).pipe(
      tap(token => {
        const expiresAt = moment().add(token.expires_in, "seconds");
        localStorage.setItem("token", JSON.stringify(token));
        localStorage.setItem("expiresAt", expiresAt.valueOf().toString());

        this.tokenSubject.next(token);
      }),
      catchError(this.handleError)
    );
  }

  logout() {
    localStorage.clear();
    this.tokenSubject.next(null);
    this.isLoggedSubject.next(false);
    this.router.navigate([""]);
  }

  handleError(error: HttpErrorResponse) {
    if (error.status == 400 && error.error.error == "invalid_grant") {
      return throwError({ failedLogin: true });
    }
    return throwError(
      'Something bad happened; please try again later.');
  };

  getAccessToken() {
    return JSON.parse(localStorage.getItem("token")).access_token;
  }
}
