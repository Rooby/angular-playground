import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { ApiService } from '../api.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { User } from 'src/app/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService extends ApiService {

  constructor(private http: HttpClient) {
    super();
   }

  list(limit: number, pageIndex: number, filter: string = "", columns: string[], sortColumn: string, sortDirection: string): any {
    const data = {
      limit: limit,
      offset: pageIndex * limit,
      filter: filter,
      columns: columns,
      sort_column: sortColumn,
      sort_direction: sortDirection
    };
    const url = this.url + "user/list";

    return this.http.post(url, data)
      .pipe(
        catchError(this.handleError)
      );
  }

  delete(id: number) {
    const url = this.url + `user/${id}`;

    return this.http.delete(url)
      .pipe(
        catchError(this.handleError)
      );
  }

  update(data: User, id: number) {
    const url = this.url + `user/${id}`;

    return this.http.put(url, data)
      .pipe(
        catchError(this.handleError)
      );
  }

  checkUniqueEmail(email: string, id: number): any {
    const data = {
      "email": email,
      "id": id
    };
    const url = this.url + `user/check-email/${id}`;

    return this.http.post(url, data)
      .pipe(
        catchError(this.handleError)
      );
  }

  handleError(error: HttpErrorResponse) {
    return throwError(
      'Something bad happened; please try again later.');
  };
}
