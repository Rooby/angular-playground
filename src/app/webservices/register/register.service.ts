import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { ApiService } from '../api.service';
import { User } from "../../models/user.model";

@Injectable({
  providedIn: 'root'
})
export class RegisterService extends ApiService {

  constructor(private http: HttpClient) {
    super();
  }

  register(data: any): Observable<User> {
    const registerURL = this.url + "user/register";
    const options = {
      headers: {
        "whitelist": "true"
      }
    };

    return this.http.post<User>(registerURL, data, options)
      .pipe(
        catchError(this.handleError)
      );
  }

  checkUniqueEmail(email: string): any {
    const data = { "email": email };
    const url = this.url + "user/check-email";
    const options = {
      headers: {
        "whitelist": "true"
      }
    };

    return this.http.post(url, data, options)
      .pipe(
        catchError(this.handleError)
      );
  }

  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  };
}
