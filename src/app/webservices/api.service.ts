import { Injectable } from '@angular/core';

import { Env } from '../config/env';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  protected url = Env.APIurl + "/api/";

  constructor() { }
}
