import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'; // CLI imports router

import { LoginComponent } from '../components/login/login.component';
import { RegisterComponent } from '../components/register/register.component';
import { DashboardComponent } from '../components/dashboard/dashboard.component';

import { AuthGuard } from '../guards/auth/auth.guard';
import { DashboardGuard } from '../guards/dashboard/dashboard.guard';

import { UsersComponent } from '../components/users/users/users.component';
import { CompaniesComponent } from '../components/companies/companies/companies.component';

const routes: Routes = [
  { 
    path: '', component: LoginComponent,
    canActivate: [DashboardGuard]
  },
  { 
    path: 'register', component: RegisterComponent,
    canActivate: [DashboardGuard]
  },
  { 
    path: 'dashboard', component: DashboardComponent,
    canActivate: [AuthGuard]
  },
  { 
    path: 'users', component: UsersComponent,
    canActivate: [AuthGuard]
  },
  { 
    path: 'companies', component: CompaniesComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
